/coinpayments_integration:
  get:
    tags:
      - Guides
    summary: Integration with Coinpayments
    description: |
      With Coinpayments you can enable deposit and withdraw of cryptocurrencies for your TokenD system.

      ### Set up your Coinpayments account

      1. Create an account and complete KYC verification
      2. Create an API key:
        - Go to "Account" menu and select "API keys"
        - Create a key by clicking on "Generate new key" button
        - Once the key is created click "Edit permissions" button and check all actions
      3. Go to "Coin Settings" and select coins you want to process in TokenD

      ### Configure Coinpayments integration service 
      1. In `coinpayments` configuration section you must fill `public_key` and `private_key` attributes with the corresponding values of your Coinpayments API key created before
      2. For each service – `deposit`, `deposit-verify` and `withdraw`, – you must specify `signer` as a secret seed of your TokenD master account and `source` as it's account ID

      > In Developer edition files to edit are `configs/coinpayments-deposit-verify.yaml`, `configs/coinpayments-deposit.yaml` and `configs/coinpayments-withdraw.yaml`


      ### Set up assets in TokenD
      Every coin must be represented as an asset in your system. Follow this guide to create one:
      1. Sign in to admin panel
      2. Pick "Assets" section and click "Create asset" button
      3. Fill in the form:
        - "Asset code" – must be a cryptocurrency code as it is displayed in Coinpayments, **can't be changed later**
        - "Maximum assets" – must be `9223372036853` (maximum possible amount)
        - "Initial preissued amount" – must be `9223372036853` (maximum possible amount)
        - "Withdrawable" – must be checked to enable withdraw, can be changed later
        - "Use Coinpayments" in "Advanced" section – must be checked
        - "External system type" in "Advanced" section - must be `0`
      4. Click "Create asset" button below the form and confirm your action

      ### How to deposit
      1. In client app go to "Movements" and select required balance
        - If there is no balance in the required asset you can create it in "Assets" section
      3. Click "Deposit" button
      4. Enter amount of deposit and click "Request the address"
      5. Send crypto to given address. Once funds are received and confirmed the amoun will be available in your balance in TokenD as well as in Coinpayments

      ### How to withdraw
      1. In client app go to "Movements" and select required balance
      2. Click "Withdraw" button
      3. Fill in amount and address and confirm the action