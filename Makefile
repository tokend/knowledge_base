all: build/openapi.yaml
	redoc-cli bundle build/openapi.yaml -t ./html/index.hbs -o ./public/index.html \
    --options.item-types-instead-of-operations=true \
        --options.root-param-name-as-group-header=true \
        --options.hide-download-button=true \
        --options.hide-response-samples=true \
        --options.hide-path=true \
        --options.flatten-response-view=true \
        --options.crop-arm-prefixes=true \
        --options.code-samples-instead-of-request-samples=true \
        --options.theme.params.underlined-header.text-transform=none \
		--options.theme.typography.links.color=#695de4 \
		--options.theme.typography.font-size=16px \
		--options.theme.right-panel.background-color=#f5f6f9 \
		--options.theme.right-panel.width=30% \
    /

sp :=

sp += # add space

my-sort = $(shell echo $(subst $(sp),'\n',$2) | sort $1 --key=1,1 -)

define appendFilesWithTabs
	for file in $(call my-sort,,$(1)); do \
		while IFS= read -r line || [ -n "$$line" ]; do \
			echo "$(2)$$line"; \
		done < $$file; \
	done >> $3
endef

build/paths.yaml: ./paths/*.yaml
	echo "paths:" > $@
	$(call appendFilesWithTabs, $^,""  "", $@)

build/openapi.yaml: index.yaml build/paths.yaml
	echo "#This file has been generated automatically. DO NOT MODIFY!" > $@
	for file in $^; do \
		while IFS= read -r line; do \
			echo "$$line"; \
		done < $$file; \
	done >> $@

clean:
	rm -rf build/*
